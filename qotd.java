/* Internet Quote of day AKA Cookie server */
/* Free for personal use                   */
/* If you don't know what this is --       */
/*  read RFC 865                           */
/*                                         */
/* Tested on OS/2 WARP running             */
/* JAVA version "JDK 1.0.2 IBM build o102-19970319 */

/* Copyright (C) hsn@cybermail.net, free for personal use. */

import java.io.*;
import java.util.*;
import java.net.*;

class qotd 
{
           public  static Hashtable quotes=new Hashtable();
           public  static volatile boolean running=true;
           public  static volatile int count=0;
           private static String copyright="Copyright (C) hsn@cybermail.net, free for personal use.";
           public static void main(String[] args)
/*          throws java.io.IOException */
                   {
                       System.out.println("Quote of the Day server - RFC 865.");
                       System.out.println(copyright);
                       System.out.println("");
                       
                       if (args.length==0) {readquotes("qotd.txt");}
                                   else {readquotes(args[0]);}
                       if (count==0) { System.out.println("No quotes loaded. Stop.");
                                       System.exit(1);
                                    }  
                       new DaemonStopper().start();             
                       TCPserver(17);
                       /*
                       for(int i=1;i<count;i++)
                        System.out.println(i+">"+quotes.get(new Integer(i))); */
                        
                   }
  private static void readquotes(String filename)
  {
    FileInputStream fis;
    DataInputStream dis;
    BufferedInputStream bis;
    long loadtime;
    
    loadtime=System.currentTimeMillis();
    try {
        fis=new FileInputStream(filename);
        bis=new BufferedInputStream(fis);
        dis=new DataInputStream(bis);
        String line = "";
        String quote_text = "";
        System.out.println("Loading quotes from: "+filename);
        while ( (line = dis.readLine())	!= null)
          {
            if (line.length()==0) { quote_text +=  "\n";continue;}

            if (line.charAt(0)=='$')
             { quotes.put(new Integer(count),new String(quote_text));
               quote_text=line.substring(1);count++;continue;}
            quote_text +=  "\n" + line ;
          }
        /* pridame posledni radek */
        quotes.put(new Integer(count),new String(quote_text));
        count++;          
        } 
    catch (FileNotFoundException fnfe)
	{
		  System.err.println("The file <" + filename + "> wasn't found.");
	}
    catch (IOException ioe)
        {
        	  System.err.println("Read error on <" + filename + ">.");
        }    
   System.out.println(count + " quotes read in "+(System.currentTimeMillis()-loadtime)/1000 + " sec");

  }
  
                   
  private static void TCPserver(int port)
  {
  ServerSocket serverSocket = null;
  try {  serverSocket = new ServerSocket(port); }
  catch (IOException e) {
                   System.out.println("Could not listen on port: " + port + ", " + e);
                   System.exit(1);
                        }
  System.out.println("Server is running on TCP/"+port);
  System.gc();        
  /* accept clients */
  while(running) /* infinite looooooop */
  {
   Socket clientSocket = null;
            try {
                clientSocket = serverSocket.accept();
            } catch (IOException e) {
                System.err.println("Accept failed: " + port + ", " + e.getMessage());
                continue;
            }
            new TCPClientThread(clientSocket).start();
  } /* loop */
  try {
      serverSocket.close();                      
      }  catch (IOException e) {}    
      
  } /* TCPserver */
}

class TCPClientThread extends Thread {
    Socket socket = null;

    TCPClientThread(Socket socket) {
        super("TCPClientThread");
        this.socket = socket;
    }

    public void run() {
        try {
            PrintStream os = new PrintStream(
                              new BufferedOutputStream(socket.getOutputStream(), 1024), false);
                              
            os.print(qotd.quotes.get(new Integer((int)java.lang.Math.round(java.lang.Math.random()*(qotd.count-1)))));                  
            os.flush();                  
            os.close();
            socket.close();
/* FOR JAVA MEMORY DEBUG ONLY! Removes LAST KEY */
/*          if(null!=qotd.quotes.remove(new Integer(qotd.count-1))) qotd.count--;*/
/*          this.yield(); */
        } catch (IOException e) {
/*            e.printStackTrace(); */
        }
  }      
}
/* KEY WATCHER !!!!!!! */

class DaemonStopper extends Thread {

    DaemonStopper() {
        super("DaemonStopper");
    }

    public void run() {
        int n;   
        System.out.println("To Quit daemon type Q\nTo force gc type G");
        while(true)
        {
        try {
             n=System.in.read();
             if (n=='Q') { qotd.running=false;
                          System.out.println("After servicing next client daemon will stop.");
                          return;
                         } 
             
             if (n=='G') {                       
                            System.gc();        
                         } 
                         
        } catch (IOException e) { }
        } /* while */
  }      
}