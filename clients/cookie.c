/* Cookie client */

#include <io.h>
#include "socket.c"

int main(int argc,char *argv[])
{
 int c,port;
 char buf[512];
 char *host;
 struct servent *s=getservbyname("qotd","tcp");
 if(s==NULL) port=17;
  else
   port=htons(s->s_port);
 if(argc!=2) { host="localhost";}
   else
     host=argv[1];
 
 c=Socket(host,port);
 if(c==-1) { perror("Connect"); return 1;}
 setmode(c,O_TEXT);
 while((port=read(c,buf,512))>0)
  write(STDOUT_FILENO,buf,port);
 close(c);
 return 0;
}